package INF101.exam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SpreadOut {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(1,3,4,7,9));
		System.out.println("Input is: "+list);
		System.out.println("Your output is: "+spread(3, list));
		System.out.println("Correct output is: "+Arrays.asList(1,4,9));
	}

	//Here we are guessing the min distance between positions and call maxNum() helper function
	//We can than decide if the guess was too large or too low.
	//We use this information to do a binary search and get O(n (log n + log M)) where M is the 
	//difference between highest and lowest number.
	public static ArrayList<Integer> spread(int k, ArrayList<Integer> list) {
		Collections.sort(list);
		int lower = 0;
		int upper = (list.get(list.size()-1)-list.get(0))/(k-1);
		ArrayList<Integer> best = new ArrayList<>();
		while(upper>lower) {
			int mid = (int)Math.ceil((upper+lower)/2.0);
			//System.out.println(lower+" < "+mid+ " < "+upper);
			ArrayList<Integer> found = maxNum(list,mid);
			if(found.size()>=k) {
				lower = mid;
				best = found;
			} else {
				upper = mid-1;
			}
		}
		while(best.size()>k)
			best.remove(best.size()-1);
		return best;
	}
	
	//This method is given a minimum distance there need to be between the positions
	//Then one can just pass through and take the first position that is more than minDist
	//from previous position
	private static ArrayList<Integer> maxNum(ArrayList<Integer> list, int minDist){ //O(n)
		ArrayList<Integer> ans = new ArrayList<>();
		ans.add(list.get(0));
		for(Integer i : list) {
			if(i>=ans.get(ans.size()-1)+minDist) {
				ans.add(i);
			}
		}
		return ans;
	}
}
