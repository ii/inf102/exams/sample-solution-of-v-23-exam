package INF101.exam;

import java.util.ArrayList;

public class FuelAverageSlow implements IAverage{

	ArrayList<Integer> history;
	int k;

	public FuelAverageSlow(int k) {
		if(k<=0)
			throw new IllegalArgumentException("Can only construct with positive argument");
		this.k =k; 
		history = new ArrayList<Integer>(k);
	}
	
	@Override
	public void add(int measurement){
		history.add(measurement); //history keeps growing, will run out of memory
	}
	
	@Override
	public double average() { //takes O(k) to compute average
		if(history.isEmpty())
			throw new IllegalStateException("Can not compute average before data has been added.");
		else {
			int sum = 0;
			int num = Math.min(k, history.size());
			for(int i=1; i<=num; i++) {
				
				sum += history.get(history.size()-i);
			}
			return sum/(double)num;
		}
	}
}