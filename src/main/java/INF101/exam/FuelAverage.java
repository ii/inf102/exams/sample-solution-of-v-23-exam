package INF101.exam;

import java.util.LinkedList;

public class FuelAverage implements IAverage {

	private int k;
	private LinkedList<Integer> history;
	private int sum;
	
	public FuelAverage(int k) {
		if(k<=0)
			throw new IllegalArgumentException("Can only construct with positive argument");
		this.k = k;
		history = new LinkedList<>();
	}

	@Override
	public void add(int measurement) {
		history.addLast(measurement);
		sum += measurement;
		if(history.size()>k) {
			sum -= history.removeFirst();
		}
	}

	@Override
	public double average() {
		if(history.isEmpty())
			throw new IllegalStateException("Can not compute average before data has been added.");
		return sum/(double)history.size();
	}
}
